#Terraform v0.14.7

variable "ssh_rsa" {
  type    = string
  default = "you public key"
}

variable "ssh_key_name" {
  type    = string
  default = "name-public-key"
}

variable "instace_type" {
  type    = string
  default = "t3.micro"
}

variable "count_instance" {
  type    = string
  default = "3"
}

variable "default_ingress" {
  type = map(object({ description = string, cidr_blocks = list(string) }))
  default = {
    22   = { description = "Inbound port to SSH", cidr_blocks = ["0.0.0.0/0"] }
    80   = { description = "Inbound port to HTTP", cidr_blocks = ["0.0.0.0/0"] }
    443  = { description = "Inbound port to HTTPS", cidr_blocks = ["0.0.0.0/0"] }
    5432 = { description = "Inbound port to Postgres", cidr_blocks = ["0.0.0.0/0"] }
    8080 = { description = "Inbound port to Tomcat", cidr_blocks = ["0.0.0.0/0"] }
  }
}

variable "Ambientes" {
  type        = map(string)
  description = "Mapa que define em qual Ambiente estamos trabalhando"
  default = {
    default = "Ambiente Padrão"
    dev     = "Ambiente de Desenvolvimento"
    hom     = "Ambiente de Homologação"
    prod    = "Ambiente de Produção"
  }
}

variable "Ambientes_regiao" {
  type = map(string)
  default = {
    default = "us-east-1"
    dev     = "us-east-1"
    hom     = "sa-east-2"
    prod    = "sa-east-1"
  }
}

locals {
  region  = lookup(var.Ambientes_regiao, terraform.workspace)
  exp_env = lookup(var.Ambientes, terraform.workspace)
}

output "Ambiente_atual" {
  value = local.exp_env
}