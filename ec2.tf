#Terraform v0.14.7

provider "aws" {
  region = local.region
}

resource "aws_key_pair" "webserver_pem" {
  key_name   = var.ssh_key_name
  public_key = var.ssh_rsa
}

resource "aws_instance" "webserver" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instace_type
  key_name      = var.ssh_key_name
  count         = var.count_instance
  credit_specification {
    cpu_credits = "standard"
  }
  tags = {
    Name = "webserver${count.index}"
  }
  security_groups = [aws_security_group.sg-webserver.name]
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] #Canonical
}

