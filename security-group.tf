#Terraform v0.14.7

resource "aws_security_group" "sg-webserver" {
  name        = "SG-dynamic"
  description = "SG com dynamic-block"

  dynamic "ingress" {
    for_each = var.default_ingress
    content {
      description = ingress.value["description"]
      from_port   = ingress.key
      to_port     = ingress.key
      protocol    = "tcp"
      cidr_blocks = ingress.value["cidr_blocks"]
    }
  }
  tags = {
    Name = "sg-webserver"
  }
}