output "public_ip" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_instance.webserver.*.public_ip
}

output "id_instance" {
  description = "List of IDs of instances"
  value       = aws_instance.webserver.*.id
}

output "public_dns" {
  description = "List of public DNS names assigned to the instances. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
  value       = aws_instance.webserver.*.public_dns
}