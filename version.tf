terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.50.0, < 3.36.0"
    }
  }
}
